Rails.application.routes.draw do
  controller :products do
    get '/', action: :index, as: :index
  end

  resources :products, path: '/produtos'
  resources :available_products, path: '/detalhes', only: [:index, :new, :create]
end
