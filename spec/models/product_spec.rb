require 'rails_helper'

describe Product, type: :model do
  context '#attributes' do
  it { should respond_to(:name) }
  it { should respond_to(:description) }
  end

  context '#validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
  end
end
