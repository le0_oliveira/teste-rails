require 'rails_helper'

describe Color, type: :model do
  context '#attributes' do
    it { should respond_to(:name) }
  end

  context '#validations' do
    it { should validate_presence_of(:name) }
  end
end
