FactoryBot.define do
  factory :color, class: Color do
    name     { 'Azul' }
  end
end