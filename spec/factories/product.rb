FactoryBot.define do
  factory :product, class: Product do
    name        { 'Azul' }
    description { 'Descrição teste' }
  end
end