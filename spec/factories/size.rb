FactoryBot.define do
  factory :size, class: Size do
    name     { 'P' }
  end
end