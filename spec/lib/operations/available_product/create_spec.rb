require 'rails_helper'

describe Operations::AvailableProduct::Create, type: :operation do
  let(:operation) { described_class }
  let(:color) { create(:color) }
  let(:size) { create(:size) }
  let(:product) { create(:product) }

  let(:valid_attributes) {{
    colors: { id: color.id },
    products: { id: product.id },
    sizes: { id: size.id }
  }}

  describe '#perform' do
    context 'with valid data' do
      it 'create product correctly' do
        created = operation.perform(valid_attributes)
        expect(created).to_not be_nil
      end
    end
  end
end