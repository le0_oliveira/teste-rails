require 'rails_helper'

describe Operations::Product::Update, type: :operation do
  let(:operation) { described_class }
  let(:product) { create(:product) }
  let(:valid_attributes) {{
    name: 'Novo nome',
    description: 'Nova descrição'
  }}

  describe '#perform' do
    context 'with valid data' do
      it 'cancel product correctly' do
        prod = operation.perform(product.id, valid_attributes)
        expect(prod.name).to eq('Novo nome')
        expect(prod.description).to eq('Nova descrição')
      end
    end
  end
end
