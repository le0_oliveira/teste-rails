require 'rails_helper'

describe Operations::Product::Create, type: :operation do
  let(:operation) { described_class }
  let(:color) { create(:color) }
  let(:size) { create(:size) }

  let(:valid_attributes) {{
    name: 'Produto novo',
    description: 'Description modelo'
  }}

  describe '#perform' do
    context 'with valid data' do
      it 'create product correctly' do
        created = operation.perform(valid_attributes, color.id, size.id)
        expect(created).to_not be_nil
      end
    end
  end
end