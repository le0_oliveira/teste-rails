class CreateAvailableProducts < ActiveRecord::Migration
  def change
    create_table :available_products, id: :uuid do |t|
      t.references :product, type: :uuid, null: false, index: true
      t.references :color, type: :uuid, null: false, index: true
      t.references :size, type: :uuid, null: false, index: true

      t.timestamps null: false
    end
  end
end
