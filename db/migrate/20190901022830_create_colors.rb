class CreateColors < ActiveRecord::Migration
  def change
    create_table :colors, id: :uuid do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
