Color.create(
  [
    { name: 'Azul' },
    { name: 'Verde' },
    { name: 'Vermelho' },
    { name: 'Amarelo' },
    { name: 'Preto' },
    { name: 'Branco' }
  ]
)

Size.create(
  [
    { name: 'P' },
    { name: 'M' },
    { name: 'G' }
  ]
)
