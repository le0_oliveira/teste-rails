module Operations
  module AvailableProduct
    class Create
      include Operations::Persistence

      self.model = ::AvailableProduct
      persist :colors, :products, :sizes

      def initialize(attributes)
        @attributes = attributes
      end

      def perform
        self.class.build @attributes do |att|
          att.product_id = @attributes[:products][:id]
          att.color_id = @attributes[:colors][:id]
          att.size_id = @attributes[:sizes][:id]
          att.save!
        end
      rescue ActiveRecord::RecordInvalid => e
        if name_taken?(e.record.errors)
          model.find_by(name: e.record.name)
        else
          raise e
        end
      end

      class << self
        def perform(attributes)
          self.new(attributes).perform
        end
      end
    end
  end
end
