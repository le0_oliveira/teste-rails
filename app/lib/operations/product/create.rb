module Operations
  module Product
    class Create
      include Operations::Persistence

      self.model = ::Product
      persist :name, :description

      def initialize(attributes)
        @attributes = attributes
      end

      def perform(color_id, size_id)
        self.class.build @attributes do |att|
          att.save!

          ::AvailableProduct.create({
            product_id: att.id,
            color_id: color_id,
            size_id: size_id
          })
        end
      rescue ActiveRecord::RecordInvalid => e
        if name_taken?(e.record.errors)
          model.find_by(name: e.record.name)
        else
          raise e
        end
      end

      class << self
        def perform(attributes, color_id, size_id)
          self.new(attributes).perform(color_id, size_id)
        end
      end
    end
  end
end
