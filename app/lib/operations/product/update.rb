module Operations
  module Product
    class Update
      include Operations::Persistence

      persist :name, :description
      self.model = ::Product

      def self.perform(id, params)
        product = model.find(id)

        product.tap do |prod|
          prod.assign_attributes(params)
          prod.save!
        end
      rescue ActiveRecord::RecordInvalid => e
        raise e.record
      end
    end
  end
end