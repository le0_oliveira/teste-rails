module Operations
  module Persistence
    extend ActiveSupport::Concern

    included do
      @_persist_params = []
      @_model = nil
    end

    module ClassMethods
      def persist(*params)
        params.each { |p| @_persist_params.push(p) }
      end

      def persist_params(params)
        sanitize_params(params).permit @_persist_params
      end

      def model=(model)
        @_model = model
      end

      def model
        @_model
      end

      def build(attributes, &block)
        instance = model.new(persist_params(attributes))
        block.call(instance) unless block.nil?
        instance
      end

      def sanitize_params(params)
        params.is_a?(Hash) ? ::ActionController::Parameters.new(params) : params
      end
    end
  end
end
