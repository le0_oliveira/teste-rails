class ProductsController < ApplicationController
  def index
    @products = Product \
                .select('products.*', 'colors.name as color', 'sizes.name as size', 'count(*) as total')
                .joins(:colors, :sizes)
                .group('products.id', 'colors.id', 'sizes.id').all
  end

  def new
    @product = Product.new
    @colors = Color.all
    @sizes = Size.all
  end

  def edit
    @product = Product.find(params[:id])
  end

  def create
    @product = ::Operations::Product::Create.perform(product_params, product_params[:colors][:id], product_params[:sizes][:id])

    redirect_to '/'
  end

  def update
    @product = ::Operations::Product::Update.perform(params[:id], product_params)

    redirect_to '/'
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
 
    redirect_to '/'
  end

  private

  def product_params
    params.required(:product).permit :name, :description, :colors => [ :id ], :sizes => [ :id ]
  end
end
