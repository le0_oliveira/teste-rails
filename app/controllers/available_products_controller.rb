class AvailableProductsController < ApplicationController
  def new
    @available = AvailableProduct.new
    @products = Product.all
    @colors = Color.all
    @sizes = Size.all
  end

  def create
    @product = ::Operations::AvailableProduct::Create.perform(available_product_params)

    render json: @product
    return

    redirect_to '/'
  end

  private

  def available_product_params
    params.required(:available_product).permit :colors => [ :id ], :products => [ :id ], :sizes => [ :id ]
  end
end
