class Product < ActiveRecord::Base
  has_and_belongs_to_many :colors, join_table: :available_products
  has_and_belongs_to_many :sizes, join_table: :available_products

  validates :name, presence: true
  validates :description, presence: true
end
